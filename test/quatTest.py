# -*- coding: utf-8 -*-
"""
Created on Mon Jun 30 14:58:04 2014

@author Thomas L. Blanchet <tlblanchet@gmail.com>
@version 1.0

NOTE: This collection of unit tests depends on an open-source testing library
  by Tom Blanchet called tomTest, which can be found here:
  https://github.com/FrogBomb/tomTest


Copyright 2014 Thomas L. Blanchet

This file is part of the quatmath library.

This quatmath library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The quatmath library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License (LGPL)
along with the quatmath library.  If not, see <http://www.gnu.org/licenses/>.
"""

from quatmath import *
import tomTest as tt

    
@tt.toTest()
def testBasicClassMethods():
    i = quat.new_i()
    j = quat.new_j()
    k = quat.new_k()
    IPlusJ = i+j
    JMinusK = j-k
    isTrue = IPlusJ != JMinusK
    isAlsoTrue = IPlusJ + k == i + (j + k)
    assert isTrue, "isTrue IS NOT TRUE!!!"
    assert isAlsoTrue, "isAlsoTrue IS NOT TRUE!!!"
#    print i, j, i*j
    assert i*j == k, "mul has a big problem!!!"
    assert i*(JMinusK) == j+k, "mul has a different big problem!!"
    return
    

@tt.toTest(10)
@tt.toTest(3.43)
@tt.toTest(0)
#@tt.toTest(True)
#@tt.toTest(None)
def testReals(number):
    assert number == quat(realPart = number)

@tt.toTest([lambda a, b: (a.one(), b.one())])
def testInheritance(testFunctions = []):
    class newQuat(quat):
        @classmethod
        def _versor(cls):
            return newVersor
    
    class newVersor(newQuat, versor):
        pass
    
    for tF in testFunctions:
        tF(newQuat, newVersor)


def testClassMethods(inQuat):
    @tt.toTest(inQuat, 2.0, [1., 0., 0.])
    def test_fromAngleAxis(inQuat, *args, **kwargs):
        return inQuat.fromAngleAxis(*args, **kwargs)
        
    @tt.toTest(inQuat, [1., 2., 3., 4.])
    def test_fromQuatVector(inQuat, *args, **kwargs):
        return inQuat.fromQuatVector(*args, **kwargs)
        
    @tt.toTest(inQuat, 1., [2., 3., 4.])
    def test_fromRealImagParts(inQuat, *args, **kwargs):
        return inQuat.fromRealImagParts(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_new_i(inQuat, *args, **kwargs):
        return inQuat.new_i(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_new_j(inQuat, *args, **kwargs):
        return inQuat.new_j(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_new_k(inQuat, *args, **kwargs):
        return inQuat.new_k(*args, **kwargs)
    
    @tt.toTest(inQuat, [2., 3., 4.])
    def test_newPureImaginary(inQuat, *args, **kwargs):
        return inQuat.newPureImaginary(*args, **kwargs)
        
    @tt.toTest(inQuat, 1.)
    def test_newRealValued(inQuat, *args, **kwargs):
        return inQuat.newRealValued(*args, **kwargs)
    
    @tt.toTest(inQuat)
    def test_one(inQuat, *args, **kwargs):
        return inQuat.one(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_zero(inQuat, *args, **kwargs):
        try:
            return inQuat.zero(*args, **kwargs)
        except ZeroQuatError:
            pass
        
testClassMethods(quat)
testClassMethods(versor)
testClassMethods(quat.newRealValued(1.0))
testClassMethods(quat.newRealValued(2.3))
testClassMethods(Rot3D)
        
def testMethods(inQuat):
    
    @tt.toTest(inQuat)
    def test_angle(inQuat, *args, **kwargs):
        try:
            return inQuat.axis(*args, **kwargs)
        except ZeroQuatError:
            pass
        
    @tt.toTest(inQuat)
    def test_conj(inQuat, *args, **kwargs):
        return inQuat.conj(*args, **kwargs)
        

    @tt.toTest(inQuat)
    def test_imagVector(inQuat, *args, **kwargs):
        return inQuat.imagVector(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_inverse(inQuat, *args, **kwargs):
        try:
            return inQuat.inverse(*args, **kwargs)
        except ZeroQuatError:
            pass
        
    @tt.toTest(inQuat)
    def test_isPureImaginary(inQuat, *args, **kwargs):
        return inQuat.isPureImaginary(*args, **kwargs)
        
    @tt.toTest(inQuat, quat([1, 2, 3, 4]))
    def test_isQuatAxisParallel(inQuat, *args, **kwargs):
        return inQuat.isQuatAxisParallel(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_isRealValued(inQuat, *args, **kwargs):
        return inQuat.isRealValued(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_isZero(inQuat, *args, **kwargs):
        return inQuat.isZero(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_log(inQuat, *args, **kwargs):
        return inQuat.log
        
    @tt.toTest(inQuat)
    def test_norm(inQuat, *args, **kwargs):
        return inQuat.norm(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_sqNorm(inQuat, *args, **kwargs):
        return inQuat.sqNorm(*args, **kwargs)
        
    @tt.toTest(inQuat, 2.1)
    def test_pow(inQuat, *args, **kwargs):
        return inQuat.pow(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_quatMatrix(inQuat, *args, **kwargs):
        return inQuat.quatMatrix(*args, **kwargs)
        
    @tt.checkIsOutput(inQuat[:], inQuat)
    def test_quatVector(inQuat, *args, **kwargs):
        return inQuat.quatVector(*args, **kwargs)
    
    @tt.toTest(inQuat)
    def test_realPart(inQuat, *args, **kwargs):
        return inQuat.realPart(*args, **kwargs)
    
    @tt.toTest(inQuat)
    def test_scaling(inQuat, *args, **kwargs):
        return inQuat.scaling(*args, **kwargs)
        
    @tt.toTest(inQuat)
    def test_toNearZeroRounded(inQuat, *args, **kwargs):
        return inQuat.toNearZeroRounded(*args, **kwargs)
    
    @tt.toTest(inQuat)
    def test_toUnitary(inQuat, *args, **kwargs):
        try:
            return inQuat.toUnitary(*args, **kwargs)
        except ZeroQuatError:
            pass
        
testMethods(quat.new_k())
testMethods(quat.new_j())
testMethods(quat.new_i())
testMethods(quat.one())
testMethods(quat.zero())
testMethods(quat([1, 1, 1, 1]))

def testOverloaded(inQ, inS, inV, inC, inN, testVector = None):
    
    @tt.toTest(inQ, inS, inV, inC)
    def test_addition(q, s, v, c):
        assert q + s == s + q
        assert(q + s) + v == q + (s + v)
        assert c + q == q + c
        return q + s, q + c
    
    @tt.toTest(inQ, inS, inV, inC)    
    def test_subtraction(q, s, v, c):
        assert q - s == -(s - q)
        assert (- q - s) - v == - q - (s + v)
        assert c - q == -(q - c)
        return q - s, c - q, q - c
        
    @tt.toTest(inQ)    
    def test_negation(q):
        return -q
            
    @tt.toTest(inQ, inS, inV, inC)
    def test_mul(q, s, v, c):
        assert c * q == q * c
        assert (q * s) * v == q * (s * v)
        assert q * (s + v) == (q * s) + (q * v)
        return q * s, q * c
            
    @tt.toTest(inQ, inS, inV, inC)    
    def test_div(q, s, v, c):
        assert s == (s / q) * q
        assert q == (q / s) * s
        assert (q / s) / v == q / (v * s)
        assert q / (s / v) == (q * v) / s
        assert q / s == (s / q).inverse()
        assert q / c == (c / q).inverse()
        return q / s, q / c
            
    @tt.toTest(inQ, inC, inN)    
    def test_exp(q, c, n):
        return q ** c, q ** n
    
    @tt.toTest(inQ)
    def test_bool(q):
        return bool(q)
        
    @tt.toTest(inQ)
    def test_len(q):
        return len(q)
    
    if testVector == None:    
        @tt.toTest(inQ, 0)
        @tt.toTest(inQ, 1)
        @tt.toTest(inQ, 2)
        @tt.toTest(inQ, 3)
        def test_indexing(q, n):
            return q[n]
    else:
        @tt.checkIsOutput(testVector[0], inQ, 0)
        @tt.checkIsOutput(testVector[1], inQ, 1)
        @tt.checkIsOutput(testVector[2], inQ, 2)
        @tt.checkIsOutput(testVector[3], inQ, 3)
        def test_indexing(q, n):
            return q[n]
    
    if testVector == None:    
        @tt.toTest(inQ, 0, 1)
        @tt.toTest(inQ, 0, 2)
        @tt.toTest(inQ, 0, 3)
        @tt.toTest(inQ, 0, 4)
        @tt.toTest(inQ, 1, 2)
        @tt.toTest(inQ, 1, 3)
        @tt.toTest(inQ, 1, 4)
        @tt.toTest(inQ, 2, 3)
        @tt.toTest(inQ, 2, 4)
        @tt.toTest(inQ, 3, 4)
        def test_slicing(q, n, m):
            return q[n:m]
    else:
        @tt.checkIsOutput(testVector[0:1], inQ, 0, 1)
        @tt.checkIsOutput(testVector[0:2],inQ, 0, 2)
        @tt.checkIsOutput(testVector[0:3],inQ, 0, 3)
        @tt.checkIsOutput(testVector[0:4],inQ, 0, 4)
        @tt.checkIsOutput(testVector[1:2],inQ, 1, 2)
        @tt.checkIsOutput(testVector[1:3],inQ, 1, 3)
        @tt.checkIsOutput(testVector[1:4],inQ, 1, 4)
        @tt.checkIsOutput(testVector[2:3],inQ, 2, 3)
        @tt.checkIsOutput(testVector[2:4],inQ, 2, 4)
        @tt.checkIsOutput(testVector[3:4],inQ, 3, 4)
        def test_slicing(q, n, m):
            return q[n:m]

testOverloaded(quat.new_i(), quat.new_j(), quat.new_k(), 2.12312312312312, 0, [0, 1, 0, 0])
testOverloaded(quat([1., 2., 3., 4.]), quat([5, 6, 7, 8]), quat([1, 0, 1, 0]), -2.3, 3, [1., 2., 3., 4.])  
testOverloaded(quat([1., 2., 3., 4.]), quat([5, 6, 7, 8]), versor([8, 2, 5, 2]), 1.0, -3)  
testOverloaded(quat([1, 2, 3, 4]), quat([1, 0, 0, 0]), quat([5, 6, 7, 8]), 1.3, 5, [1, 2, 3, 4])    
testOverloaded(quat([1, 2, 3, 4]), quat([5, 6, 7, 8]), quat([1, 0, 1, 0]), 1.3, 5)            
    
#@tt.checkIsOutput(0, .2, [0, 0, 0])
#def testAngle(angle, axisVec):
#    q = quat.fromAngleAxis(angle, np.array(axisVec))
#    return q.angle()
#
#@tt.checkIsOutput([0, 0, 0], .2, [0, 0, 0])
#@tt.checkIsOutput([0, 0, 0], 0.0, [1, 0, 0])
#def testAxis(angle, axisVec):
#    q = quat.fromAngleAxis(angle, np.array(axisVec))
#    return q.axis()