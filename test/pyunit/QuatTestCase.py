# -*- coding: utf-8 -*-
"""
PyUnit tests for quatmath.quat class.

@author Will D. Spann <willdspann@gmail.com>

Copyright 2015 Will D. Spann

This file is part of the quatmath library.

This quatmath library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The quatmath library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License (LGPL)
along with the quatmath library.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import math
import numpy as np
import quatmath as qm

class QuatTestCase(unittest.TestCase):
    
    def testAngle(self):
        angle = math.pi / 2.0
        q = qm.quat.fromAngleAxis(angle, np.array([1.0, 0., 0.]))
        expected = angle
        retAngle = q.angle()
        
        assert retAngle == expected
        
    
    def testAxis(self):
        axisVec = np.array([1.0, 0., 0.])
        q = qm.quat.fromAngleAxis(math.pi / 2.0, axisVec)
        expected = axisVec
        retAxis = q.axis()
        
        assert np.all(retAxis == expected)
        
    
    def testQuatVector(self):
        qvec = [0.5, 1.0, 2.5, 0.7]
        q = qm.quat(qvec)
        expected = qvec
        retQuatVec = q.quatVector()
        
        assert np.all(retQuatVec == expected)

if __name__ == "__main__":
        unittest.main()
