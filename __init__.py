# -*- coding: utf-8 -*-
"""
quatmath quaternion & versor library module.

Copyright 2015 Will D. Spann

This file is part of the quatmath library.

This quatmath library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The quatmath library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License (LGPL)
along with the quatmath library.  If not, see <http://www.gnu.org/licenses/>.
"""

from .quatmath import quat, versor
