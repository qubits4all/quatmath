# -*- coding: utf-8 -*-
"""
A library for doing quaternion and versor arithmetic. Provides quat & versor
classes for these purposes. The versor class supports several operations for
performing 3D rotations & representing orientation in 3D.

@author Will D. Spann <willdspann@gmail.com>,
        Thomas L. Blanchet <tlblanchet@gmail.com>
 
@version 1.1
         
Copyright 2014, 2015 Will D. Spann, Thomas L. Blanchet

This file is part of the quatmath library.

This quatmath library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The quatmath library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License (LGPL)
along with the quatmath library.  If not, see <http://www.gnu.org/licenses/>.
"""

import math
import numpy as np
import numpy.linalg as la
    
##TODO: Tom: Unit tests, complete documentation, test subclassability
## (overriding __new__ often screws with subclassing, especially with
## immutables. I think I handled this, but we should still test it),
## document subclassing requirements and expectations
## and discuss what other helper functions we need. 
    

DEF_THRESHOLD = 1.0e-10  # Default threshold for equality comparisons
NUMERIC_TYPE = (int, long, float, np.float32, np.float64)

def _ARE_COMPS_NUM(iterable): ##ARE_ALL_COMPONENTS_OF_AN_INTERABLE_OF_NUMERIC_TYPE
    return all([isinstance(comp, NUMERIC_TYPE) for comp in iterable])
    
class ZeroQuatError(ArithmeticError):
    def __init__(self, \
                 message = "Operation not supported on zero-norm quats."): 
        self._msg = message
        
    def __str__(self):
        return repr(self._msg)


class RealQuatError(ArithmeticError):
    def __init__(self, \
                 message = "Operation not supported on real-valued quats."):
        self._msg = message

    def __str__(self):
        return repr(self._msg) 


class quat(object):
    """ A really cool quaternion class.
        To Use:
        
            The following factory functions should be used to generate
            a quat:
                one()   --  Generates the unit quat
                zero()  --  Generates the zero quat
                new_i() --  Generates i, a quat base element
                new_j() --  Generates j, a quat base element
                new_k() --  Generates k, a quat base element
                
                newPureImaginary(imagVector)
                    --  Generates a pure imaginary quat from an iterable.
                        (length 3, [i_coef, j_coef, k_coef])
                            
                newRealValued(real)
                    --  Generates a pure real quat from a numeric.
                            
                fromRealImagParts(real, imagVector)
                    --  Generates a quat with a specified real numeric part and
                        an imaginary part from an iterable.
                        (length 3, [i_coef, j_coef, k_coef])
                            
                fromQuatVector(quatVector)
                    --  Generates a quat directly from an iterable.
                        (length 4, [real_coef, i_coef, j_coef, k_coef])
                        
                fromAngleAxis(angle, axisVector, scaling = 1.0)
                    --  Generates a quat that is representative of a
                        4-dimentional rotation with a scaling factor.
                        Specifically, when the axisVector is normalized, the
                        quat returned is equal to:
                            
                        scaling * (e ** (angle * axisVector))
                        
                        (If axisVector is not normalized, it will be
                        normalized if possible.)
                
                fromQuatMatrix(matrix)
                    --  Generates a quat from a 4x4 matrix (if possible).
                 
            
            The following operations are availible for quats q and s,
            integers n and m, and a general numeric c:
            
                addition        ----    q + s,  c + q
                subtraction     ----    q - s,  q - c, c - q
                negation        ----    -q
                multiplication  ----    q * s,  c * q
                division        ----    q / s,  c / s,  s / c
                modulus         ----    q % n, q % c
                exponentiation  ----    q ** c,  q ** n
                                            Note: q ** s is not implemented.
                zero testing    ----    bool(q): False if close to zero, True
                                            otherwise.
                length testing  ----    len(q): q.quatVector() length, i.e. 4.
                indexing        ----    q[n]: 0 -> real part, 1 -> i coef,
                                            2 -> j coef, 3 -> k coef
                slicing         ----    q[m:n]: Returns sequence of
                                            coefficients w/ indices from
                                            m inclusive to n exclusive.
            

            When subclassing quat, please override _versor() with
            the classmethod decorator (@classmethod before
            def _versor())if you want a different behavor 
            when passing arguements for a unit-norm quaternion.
                                
            Methods are defined and described below.
            
            
        """
        
    
        
        
    def __init__(self, \
                 quatVector = None, \
                 inQuat = None, \
                 realPart = None, \
                 imagVec = None, \
                 angle = None, \
                 axisVec = None, \
                 scaling = None):

        self._qvec      = None  # 4-vector of 1 real & 3 imaginary components
        self._scaling   = None  # scaling factor
        self._angle     = None
        self._axis      = None
        
        if quatVector != None:
            if all([isinstance(qc, NUMERIC_TYPE) \
                    for qc in quatVector]):
                
                try:
                    self._qvec = [qc for qc in quatVector]
                
                except IndexError:
                    raise TypeError("quatVector must have integer or " \
                        + "floating-point components.")
                        
            else:
                raise TypeError("quatVector must be a sequence of length 4.")
        
        elif inQuat != None:
            if isinstance(inQuat, quat):
                if inQuat._qvec != None:
                    self._qvec = [qc for qc in inQuat._qvec]
          
                elif self._angle != None:          
                    self._angle = inQuat._angle
                    self._axis = np.array([ac for ac in inQuat._axis]) # copy axis
                    
                    if inQuat._scaling != None:
                        self._scaling = inQuat._scaling
                    else:
                        self._scaling = 1.0
            elif isinstance(inQuat, NUMERIC_TYPE):
                realPart = inQuat
            else:
                raise TypeError( \
                    "quat must be an instance of quatmath.quat or a subclass.")
                                    
        elif realPart != None:
            if isinstance(realPart, NUMERIC_TYPE):
                if imagVec != None:
                    # If all components of imagVec are supported numeric types
                    if all([isinstance(i, NUMERIC_TYPE) \
                            for i in imagVec]):
                                
                        try:
                            self._qvec = [realPart] + [ic for ic in imagVec]
                            
                        except IndexError:
                            raise TypeError( \
                                "Imaginary vector must be a sequence of length 3.")
                            
                    else:
                        raise TypeError("Imaginary vector must have integer " \
                            + "or floating-point components.")
                        
                else:
                    self._qvec = [realPart, 0., 0., 0.]

            else:
                raise TypeError( \
                    "Real part must be an integer or floating-point number.")
                
        elif imagVec != None:
            if all([isinstance(ic, NUMERIC_TYPE) \
                    for ic in imagVec]):
                        
                try:
                    self._qvec = [0.] + [ic for ic in imagVec]
                    
                except IndexError:
                    raise TypeError( \
                        "Imaginary vector must be a sequence of length 3.")
            else:
                raise TypeError("Imaginary vector must have integer " \
                            + "or floating-point components.")
                
        elif angle != None and axisVec != None:
            if isinstance(angle, NUMERIC_TYPE) \
                and all([isinstance(ac, NUMERIC_TYPE) \
                         for ac in axisVec]):
                
                if scaling == None:
                    scaling = 1.0
                
                # Rotation angle is represented as the half-angle in quat.
                self._angle = 0.5 * angle
                self._scaling = scaling
            
                try:
                    self._axis = np.array([ac for ac in axisVec])  # copy axis
                
                except IndexError:
                    raise TypeError("axisVec must be a sequence of length 3.")
                    
            else:
                raise TypeError("angle and the components of axisVec must be " \
                    + "integers or floating-point numbers.")
            
        else:
            raise TypeError("Invalid parameters.")

    @classmethod    
    def _versor(cls):
        """
        Overrride this if the versor of your subclass is different
        """
        if isinstance(versor, cls):
            class newVersorType(cls, versor):
                pass
            return newVersorType
        else:
            return versor
        
    @classmethod
    def _quat(cls):
        """
        Override this if the non-versor quat superclass is different
        """
        return cls

    def __new__(cls,\
                quatVector = None, \
                inQuat = None, \
                realPart = None, \
                imagVec = None, \
                angle = None, \
                axisVec = None, \
                scaling = None,
                **kwargs):
                     
        ##newInstance will change if the input values
        ##      make this something simplier than a quat.
        ##      (e.g., int, float, etc.)
        newInstance = None
                
        ##The following if/else blocks simply check
        ##if the inputs could be a base type or a versor.
        ##cls is changed to the current classes versor type
        ##in the latter case, and newInstance is changed
        ##in the former.
        if quatVector != None:
            imgSquareSum = sum([i * i for i in quatVector[1:4]])
            if mostlyEqual(0.0, math.sqrt(imgSquareSum)):
                if isinstance(quatVector[0], NUMERIC_TYPE):
                    newInstance = quatVector[0]
            elif mostlyEqual(1.0,\
                             math.sqrt(quatVector[0] * quatVector[0] \
                                       + imgSquareSum)):
                                           
                if not isinstance(cls, cls._versor()):
                    cls = cls._versor()
                    
        elif imagVec != None:
            if realPart == None:
                realPart = 0.0
            imgSquareSum = sum([i * i for i in imagVec])
            if mostlyEqual(0.0, math.sqrt(imgSquareSum)):
                if isinstance(realPart, NUMERIC_TYPE):
                    newInstance = realPart
            elif mostlyEqual(1.0, math.sqrt(realPart*realPart+imgSquareSum)):
                if not isinstance(cls, cls._versor()):
                    cls = cls._versor()
                    
        elif realPart != None:
            newInstance = realPart
                    
        elif angle != None:
            if axisVec != None:
                axisMag = math.sqrt(sum([i * i for i in axisVec]))
                if mostlyEqual(0.0, angle):
                    if isinstance(scaling, NUMERIC_TYPE):
                        newInstance = scaling
                elif mostlyEqual(0.0, axisMag):
                    newInstance = scaling
                elif mostlyEqual(scaling, 1.0):
                    if not isinstance(cls, cls._versor()):
                        cls = cls._versor()
                    
        ##If newInstance didn't change, just make a new object of the
        ##  current class to go to __init__. 
        if newInstance == None:
            return object.__new__(cls)
                
        if mostlyEqual(abs(newInstance), 1.0):
            cls = cls._versor()
        
        ##On-the-fly doubly inherited class delclaration
        class quatNumeric(newInstance.__class__, cls):
            def __new__(cls1, *args, **kwargs):
                ##This returns a new instance of the class of newInstance, skipping
                ##the quat.__new__ processing.
                return newInstance.__class__.__new__(cls1, newInstance)
            def __init__(self, *args, **kwargs):
                ##After we get the new instance, we pass it through
                ##the cls.__init__, (cls is a subclass of quat)
                kwargs["realPart"] = newInstance
                cls.__init__(self, *args, **kwargs)
                ##then we pass the newly generated instance into class
                ##of newInstance's __init__ function,
                ##where we assume that it acts as a cast for itself.
                ##(e.g., int(1.0) makes an int with value 1)
                newInstance.__class__.__init__(self, newInstance)
            def __pow__(self, exponent):
                return cls.__pow__(self, exponent)
        
        ##We could pass things to quatNumeric for our specific newInstance, but
        ##currently everything is handled in the constructor.
        return quatNumeric()        

    
    def __str__(self):
        qvec = self.quatVector()
        retStr = "(" + (str(qvec[0]) + " ")*(qvec[0]!=0) +\
                (" " + str(qvec[1]) + "i ")*(qvec[1]!=0) +\
                (" " + str(qvec[2]) + "j ")*(qvec[2]!=0) +\
                (" " + str(qvec[3]) + "k")*(qvec[3]!=0) + ")"
        retStr = retStr.replace("  ", "+")
        retStr = retStr.replace(" ", "")
        retStr = retStr.replace("+", " + ")
        return retStr
                
    def __repr__(self):
        qvec = self.quatVector()
        return str(self.__class__.__name__) + "(" +str(qvec)+ ")"

        
    def __add__(self, q):
        if isinstance(q, quat):
            return quat._add(self, q)
        
        elif isinstance(q, NUMERIC_TYPE):
            return self._scalarAdd(q)
            
        else:
            raise TypeError( \
                "q must be an instance of quatmath.quat or a subclass.")
    
    def __radd__(self, q):
        return self + q

        
    def __neg__(self):
        if self._qvec != None:
            return self.__class__(quatVector = [-sc for sc in self._qvec])
            
        else:
            return self.__class__(angle = self._angle,\
                                  axisVec = self._axis,\
                                  scaling = -self._scaling)


    def __sub__(self, q):
        
        return self + (-q)
        
    def __rsub__(self, q):
        
        return q + (-self)
        

    def __mul__(self, q):  # self * q
        """ Right-multiplies this quaternion by the given quat q, and returns this
        product. That is, this method calculates: self * q """
        if isinstance(self, self._versor()):
            if isinstance(q, versor):
                return self._multiply(self, q)
            
            elif isinstance(q, NUMERIC_TYPE):
                return self._scalarMultiply(q)
            
            elif isinstance(q, self._quat()):
                return self._quat()._multiply(self, q)
            else:
                raise TypeError("q must be an instance of quatmath.quat or a " \
                    + "subclass, or be an integer or floating-point number.")   
        else:            
            if isinstance(q, quat):
                return self._multiply(self, q)
            
            elif isinstance(q, NUMERIC_TYPE):
                return self._scalarMultiply(q)
    
            else:
                raise TypeError("q must be an instance of quatmath.quat or a " \
                    + "subclass, or be an integer or floating-point number.")


    def __rmul__(self, q):  # q * self
        """ Left-multiplies this quaternion by the given quat q, and returns this
        product. That is, this method calculates: q * self """
        if isinstance(q, quat):
            return self._multiply(q, self)

        elif isinstance(q, NUMERIC_TYPE):
            return self._scalarMultiply(q)

        else:
            raise TypeError("q must be a quatmath.quat, or an integer or " \
                + "floating-point number.")
        
        
    def __div__(self, q):
        return self._divide(self, q)
        
    def __rdiv__(self, q):
        return self._divide(q, self)
        
    def __truediv__(self, q):
        return self._divide(self, q)
        
    def __rtruediv__(self, q):
        return self._divide(q, self)

    
    def __pow__(self, exponent, modulus = None):  # self ** exponent
        if modulus == None:
            return self.pow(exponent)
        else:
            return self.pow(exponent)%modulus
    
    def __mod__(self, modulus):
        return self._mod(self, modulus)
        
    def __nonzero__(self):
        """ Returns True if this quat's norm is not very close or equal to zero.
        This method overloads the bool(q) operator intrinsic. """
        return not self.isZero()
    

    def __len__(self):
        return 4
    
    
    def __getitem__(self, val):
        return self.quatVector()[val]

        
    def __eq__(self, other):
        if isinstance(other, (quat,) + NUMERIC_TYPE):
            difference = self-other
            if hasattr(difference, "norm"):
                return mostlyEqual(difference.norm(), 0.0)
            else:
                return mostlyEqual(difference, 0.0)
        else:
            return False
        
    def __ne__(self, other):
        return not (self == other)
        

    def quatVector(self):
        if self._qvec != None:
            return [qc for qc in self._qvec]  # make copy
        else:
            return self._calcQuatVector()


    def angle(self):
        ''' Return this quat's angle of rotation, when viewed as a
            possibly-scaled versor (i.e., rotation quaternion). '''
        
        if self._angle != None:
            # Convert the internal half-angle to actual rotation angle.
            return 2.0 * self._angle
        else:
            # Calc. quat's half-angle and return the assoc. full rotation angle.
            return 2.0 * self._calcAngle()

            
    def axis(self):
        if self._axis != None:
            return np.copy(self._axis)
        else:
            return self._calcAxis()

        
    def scaling(self):                
        if self._scaling != None:
            return self._scaling
        elif self._angle != None and self._axis != None:
            return 1.0
        else:
            assert self._qvec != None
            return self.norm()
    
    
    def realPart(self):
        if self._qvec != None:
            return self._qvec[0]
        else:
            return self._calcRealPart()


    def imagVector(self):
        if self._qvec != None:
            return np.array([ic for ic in self._qvec[1:]])
        else:
            return np.array(self._calcImagVector())
    
    # TODO: Commented out until _calcQuatMatrix() has been check for
    #   correctness.
#    def quatMatrix(self):
#        return self._calcQuatMatrix()


    def norm(self):
        return math.sqrt(self.sqNorm())
        
    def sqNorm(self):
        if self._scaling != None:
            return abs(self._scaling) # scaling can be negative (see __neg__())

        qvec = self._qvec
        if self._qvec == None:
            qvec = self._calcQuatVector()
        
        sqNorm = sum([sc * sc for sc in qvec \
                      if not mostlyEqual(sc, 0.0)])
        
        return sqNorm
        
    # Overloads the absolute value operator, so we can write: qnorm = |q|
    # Note: This operator syntax may be Python 3.x only.
    __abs__ = norm
    
    
    def conj(self):
        if self._qvec != None:
            quatVec = [self._qvec[0]] + [-sc for sc in self._qvec[1:]]
            return quat(quatVec)
            
        else:  # self._axis != None and self._angle != None:
            return quat(angle = self._angle, axisVec = -self._axis)

    
    def inverse(self):
        sqNorm = self.sqNorm()
        
        if mostlyEqual(math.sqrt(sqNorm), 0.0):
            raise ZeroQuatError("This quat's inverse does not exist, because "
                + "its norm is 0 or very near 0.")

        return self.conj() / sqNorm
        
        
    def pow(self, exponent):   
        """ Applies the given real exponent to this quaternion. """
        if isinstance(exponent, NUMERIC_TYPE):
            
            if exponent == 1.0:
                return self
            elif exponent == 0.0:
                return quat.one()
                
            if self.isZero():
                return self
            
            quatNorm = self.norm()
            qVersor = self
            
            if not mostlyEqual(quatNorm, 1.0):
                # Only normalize if non-unitary.
                qVersor = self / quatNorm
                
            imagVec = qVersor.imagVector()
            imagNorm = la.norm(imagVec)
            # Note: This is more numerically stable than math.acos(q.realPart()).
            theta = math.atan2(imagNorm, qVersor.realPart())
            newTheta = exponent * theta
            
            powRealPart = None
            powImagVec = None
            if not mostlyEqual(quatNorm, 1.0):
                qNormPow = math.pow(quatNorm, exponent)
                powRealPart = qNormPow * math.cos(newTheta)
                powImagVec = (qNormPow * math.sin(newTheta)) * imagVec
            else:
                powRealPart = math.cos(newTheta)
                powImagVec = math.sin(newTheta) * imagVec
            
            return quat(realPart = powRealPart, imagVec = powImagVec)
        
        else:
            raise TypeError( \
                "exponent must be an integer or floating-point number.")
        

    def log(self):
        """ Returns this quaternion's natural logarithm.
            This can be most simply calculated as follows:
            
                ln(q) = ln(||q||) + (v/||v||) * acos(a/||q||)
                
            However, we use a slight variation in the angle determination, for
            improved numerical stability, esp. when the angle turns out to be
            close to zero or pi.
            
                ln(q) = ln(||q||) + (v/||v||) * atan(||(v/||q||)|| / (a/||q||))
        """
        quatNorm = self.norm()
        
        if mostlyEqual(quatNorm, 0.0):
            raise ZeroQuatError()
            
        qVersor = self / quatNorm
        logRealPart = math.log(quatNorm)  # ln(quatNorm)
        imagVec = qVersor.imagVector()
        imagNorm = la.norm(imagVec)
        # Note: atan here is more numerically stable than acos(realPart), esp.
        #   when the angle is close to zero or pi.
        angle = math.atan2(imagNorm, qVersor.realPart())
        axisVec = imagVec / imagNorm
        logVecPart = angle * axisVec
        
        return quat(realPart = logRealPart, imagVector = logVecPart)
        
    
    @classmethod
    def copy(cls, toCopy):
        return cls(inQuat = toCopy)
    
    
    def toUnitary(self):
        norm = self.norm()
        
        if mostlyEqual(norm, 0.0):
            raise ZeroQuatError()
        
        return self / norm
        
        
    def toNearZeroRounded(self):
        """
        Returns a quat w/ any near-zero quat-vector components set to 0.0.
        """        
        qvec = self._qvec
        if self._qvec == None:
            qvec = self._calcQuatVector()
            
        qvecRounded = [(0.0 if mostlyEqual(qc, 0.0) else qc) for qc in qvec]
        
        return self.fromQuatVector(qvecRounded)
    
    
    def isQuatAxisParallel(self, q, threshold = DEF_THRESHOLD):
        return self._areQuatAxesParallel(self, q, threshold)
        
        
    def isRealValued(self):
        return isinstance(self, NUMERIC_TYPE)
        
        
    def isPureImaginary(self):        
        return mostlyEqual(self.realPart(), 0.0)
        
        
    def isZero(self):
        return isinstance(self, NUMERIC_TYPE) \
            and mostlyEqual(self.realPart(), 0.0)


    def _scalarMultiply(self, r):
        assert isinstance(r, NUMERIC_TYPE)        
        if mostlyEqual(r, 1.0):
            return self
        elif self._qvec != None:
            qvec = self._qvec[:]
        else:
            qvec = self._calcQuatVector()
            
        quatVec = [r * sc for sc in qvec]
        
        return quat(quatVec)
    
    def _scalarAdd(self, r):
        assert isinstance(r, NUMERIC_TYPE)        
        if mostlyEqual(r, 0.):
            return self
        elif self._qvec != None:
            qvec = self._qvec[:]
        else:
            qvec = self._calcQuatVector()
            
        qvec[0] += r
        
        return quat(qvec)
    
    def _calcAngle(self):
        if self._angle == None:
            assert self._qvec != None
            
            if self.isZero():
                raise ZeroQuatError()
            elif self.isRealValued():
                return 0.0
            
            versor = self.toUnitary()
            imagVec = versor.imagVector()
            imagNorm = la.norm(imagVec)
            angle = math.atan2(imagNorm, versor.realPart())
        
            # Return this quat's calculated half-angle.
            return angle
        else:
            # Return the internal half-angle.
            return self._angle
    
    
    def _calcAxis(self):
        if self._axis == None:
            assert self._qvec != None            
            
            if self.isZero():
                raise ZeroQuatError()
            elif self.isRealValued():
                return [0, 0, 0]
    
            versor = self.toUnitary()
            imagVec = versor.imagVector()
            
            return imagVec
        else:
            return self._axis
        
        
    def _calcAngleAxis(self):
        if self._angle == None or self._axis == None:
            assert self._qvec != None
            
            if self.isZero():
                raise ZeroQuatError()
            elif self.isRealValued():
                raise RealQuatError()
    
            qnorm = self.norm()
            versor = self.__div__(qnorm)
            imagVec = versor.imagVector()
            imagNorm = la.norm(imagVec)
            angle = math.atan2(imagNorm, versor.realPart())
            
            # Return this quat's calculated half-angle, axis of rotation
            # & scaling-factor
            return angle, imagVec, qnorm
        elif self._scale == None:
            # Return this quat's internal half-angle, axis of rotation, and its
            # calculated scaling-factor.
            return self._angle, self._axis, self.norm()
        else:
            # Return this quat's internal half-angle, axis of rotation
            # & scaling factor.
            return self._angle, self._axis, self._scale
    
    
    def _calcRealPart(self):
        if self._qvec == None:
            # Calc. & return the real-part, which is the cosine of the quat's
            # internal half-angle.
            return math.cos(self._angle)
        else:
            return self._qvec[0]
    
    
    def _calcImagVector(self):
        if self._qvec == None:
            # Calc. the imaginary part's magnitude, which is the sine of the
            # quat's internal half-angle.
            sinAngle = math.sin(self._angle)
            versorImagVec = [sinAngle * ac for ac in self._axis]
            scaledImagVec = [self._scaling * vc for vc in versorImagVec]
            
            return np.array(scaledImagVec)
        else:
            return np.array([ic for ic in self._qvec[1:]])
    
    
    def _calcQuatVector(self):
        if self._qvec == None:
            # Calc. the imaginary part's magnitude, which is the sine of the
            # quat's internal half-angle.
            sinAngle = math.sin(self._angle)
            versorVec = [math.cos(self._angle)] + [sinAngle * ac for ac in self._axis]
            quatVec = [self._scaling * vc for vc in versorVec]
            return quatVec
        else:
            return self._qvec[:]
            
            
    # TODO: Check correctness, and whether we intended this to produce a left-
    #   or right-multiplication quat matrix.
#    def _calcQuatMatrix(self):
#        ##TODO: optimize
#        realMat = np.matrix([[ 1, 0, 0, 0],\
#                             [ 0, 1, 0, 0],\
#                             [ 0, 0, 1, 0],\
#                             [ 0, 0, 0, 1]])
#                             
#        iMat    = np.matrix([[ 0, 1, 0, 0],\
#                             [-1, 0, 0, 0],\
#                             [ 0, 0, 0,-1],\
#                             [ 0, 0, 1, 0]])
#        
#        jMat    = np.matrix([[ 0, 0, 1, 0],\
#                             [ 0, 0, 0, 1],\
#                             [-1, 0, 0, 0],\
#                             [ 0,-1, 0, 0]])
#        
#        kMat    = np.matrix([[ 0, 0, 0, 1],\
#                             [ 0, 0,-1, 0],\
#                             [ 0, 1, 0, 0],\
#                             [-1, 0, 0, 0]])
#        iP = self.imagVector()
#        return realMat * self.realPart() + iMat * iP[0] + jMat * iP[1] + kMat * iP[2]


    @classmethod
    def one(cls):
        """
        Returns 1.0, the multiplicative identity.
        """
        return cls.newRealValued(1.0)
    
    @classmethod
    def zero(cls):
        """
        Returns 0.0, the additive identity.
        """
        return cls.newRealValued(0.0)
    
    @classmethod    
    def new_i(cls):
        """
        Generates i, a quat base element
        """
        return cls.newPureImaginary([1.0, 0., 0.])
    
    @classmethod
    def new_j(cls):
        """
        Generates j, a quat base element
        """
        return cls.newPureImaginary([0., 1.0, 0.])
    
    @classmethod    
    def new_k(cls):
        """
        Generates k, a quat base element
        """
        return cls.newPureImaginary([0., 0., 1.0])
    
    @classmethod
    def newPureImaginary(cls, imagVector):
        """
        Generates a pure imaginary quat from an iterable.
        (length 3, [i_coef, j_coef, k_coef])
        """
        return cls(imagVec = imagVector)
    
    
    @classmethod
    def newRealValued(cls, real):
        """
        Generates a pure real quat from a numeric.
        """
        if isinstance(real, NUMERIC_TYPE):
            return cls(realPart = real)
        else:
            raise TypeError(\
                "real must be an integer or floating-point number.")
    
    
    @classmethod
    def fromRealImagParts(cls, real, imagVector):
        """
        Generates a quat with a specified real numeric part and an imaginary
        part from an iterable.
        (length 3, [i_coef, j_coef, k_coef])
        """
        return cls(realPart = real, imagVec = imagVector)
    
    
    @classmethod
    def fromQuatVector(cls, quatVector):
        """
        Generates a quat directly from an iterable.
        (length 4, [real_coef, i_coef, j_coef, k_coef])
        """
        return cls(quatVector)
    
    
    @classmethod
    def fromAngleAxis(cls, angle, axisVector, scaling = 1.0):
        """
        Generates a quat that is representative of a
        4 dimentional rotation with a scaling factor.
        Specifically, when the axisVector is normalized, 
        the quat returned is equal to 
        scaling*(e**angle*newPureImaginary(axisVector)).
        (if axisVector is not normalized, it will be
        normalized if possible.)
        """
        return cls(angle = angle, axisVec = axisVector, scaling = scaling)

    @classmethod
    def fromVectorDivision(cls, vector1, vector2):
        """
        This factory function produces a general quaternion (which may
        represent a scaling-factor in addition to a 3D rotation), as the
        "quotient" of the two 3D vectors provided. In particular, when the
        resulting quaternion is applied (via conjugation,
        i.e.: q * dividendQ * q**-1) to a pure-imaginar quaternion formed from
        vector2 (the divisor), the resulting quaternion's imaginary part equals
        vector1 (the dividend). That is, the effect of this conjugation is to
        rotate vector2 (i.e., the divisor) to be parallel with vector1
        (i.e., the dividend), and to scale its magnitude (i.e., its 2-norm)
        to equal that of vector1 (i.e., the dividend).
        """
        if len(vector1) != 3 or len(vector2) != 3:
            raise TypeError('vector1 & vector2 must be numpy arrays '\
                + 'or sequences of length 3.')
                
        # Handle any length-3 sequence, not just numpy arrays.
        if not isinstance(vector1, np.array):
            vector1 = np.array([dc for dc in vector1])
        if not isinstance(vector2, np.array):
            vector2 = np.array([dc for dc in vector2])    
        
        norm1 = la.norm(vector1)
        norm2 = la.norm(vector2)
        normProd = norm1 * norm2
        dotProd = np.dot(vector1, vector2)
        crossProdVec = np.cross(vector2, vector1)
        cosTheta = dotProd / normProd
        sinTheta = la.norm(crossProdVec / normProd)
        angle = math.atan2(sinTheta, cosTheta)
        normalVec = crossProdVec / (normProd * sinTheta)
        
        quotUnitary = cls(angle = angle, axisVec = normalVec)
        return quotUnitary * (norm1 / norm2)  # Include any scale-factor.
    
    
    # TODO: Check correctness, and whether we intended this to convert from a
    #   left- or right-multiplication quat matrix.
#    @classmethod
#    def fromQuatMatrix(cls, quaternionMatrix):
#        qM = np.array(quaternionMatrix)
#        realPt = qM[0, 0]
#        iPt = qM[0, 1]
#        jPt = qM[0, 2]
#        kPt = qM[0, 3]
#        realMat = np.array  ([[ 1, 0, 0, 0],\
#                             [ 0, 1, 0, 0],\
#                             [ 0, 0, 1, 0],\
#                             [ 0, 0, 0, 1]])*realPt
#                             
#        iMat    = np.array  ([[ 0, 1, 0, 0],\
#                             [-1, 0, 0, 0],\
#                             [ 0, 0, 0,-1],\
#                             [ 0, 0, 1, 0]])*iPt
#        
#        jMat    = np.array  ([[ 0, 0, 1, 0],\
#                             [ 0, 0, 0, 1],\
#                             [-1, 0, 0, 0],\
#                             [ 0,-1, 0, 0]])*jPt
#        
#        kMat    = np.array  ([[ 0, 0, 0, 1],\
#                             [ 0, 0,-1, 0],\
#                             [ 0, 1, 0, 0],\
#                             [-1, 0, 0, 0]])*kPt
#        if not all(mostlyEqual(qM, realMat+iMat+jMat+kMat)):
#            raise ValueError("Must be a quaternion matrix.")
#        else:
#            return cls.fromQuatVector([realPt, iPt, jPt, kPt])
    
    
    @classmethod
    def _add(cls, q, s):
        if (not isinstance(q, quat)) or (not isinstance(s, quat)):
            raise TypeError( \
                "Both q & s must be instances of quatmath.quat or a subclass.")
        
        qQuatVec = q._qvec
        sQuatVec = s._qvec         
        if q._qvec == None or s._qvec == None:
            if q._qvec == None:
                qQuatVec = q._calcQuatVector()
            if s._qvec == None:
                sQuatVec = s._calcQuatVector()
        
        sumQuatVec = [0.] * 4
        for i in range(4):
            sumQuatVec[i] = qQuatVec[i] + sQuatVec[i]
        
        return cls._quat()(sumQuatVec)


    @classmethod
    def _multiply(cls, q, s):
        """
        Multiplies two quaternions and returns their product (another quat). 
        """
        if isinstance(q, NUMERIC_TYPE) and isinstance(s, NUMERIC_TYPE):
            return q * s
        elif isinstance(s, NUMERIC_TYPE):
            return q._scalarMultiply(s)
        elif isinstance(q, NUMERIC_TYPE):
            return s._scalarMultiply(q)
            
        elif not isinstance(q, quat) or not isinstance(s, quat):
            raise TypeError( \
                "Either q or s or both must either be instances of "\
                + "quatmath.quat, a subclass, or be integer or "\
                + "floating-point numbers.")
        
        qQuatVec = q._qvec
        sQuatVec = s._qvec
        
        # quatVector form:
        if q._qvec != None or s._qvec != None:
            if q._qvec == None:
                qQuatVec = q._calcQuatVector()
            elif s._qvec == None:
                sQuatVec = s._calcQuatVector()
            
            return cls._multiplyQuatVecForm(qQuatVec, sQuatVec)
            
        else: # Angle-axis form
            return cls._multiplyAngleAxisForm(q, s)


    @classmethod
    def _divide(cls, q, s):
        if not isinstance(q, (quat,) + NUMERIC_TYPE):
            raise TypeError("q must be an instance of quatmath.quat or a subclass.")
        if not isinstance(s, (quat,) + NUMERIC_TYPE):
            raise TypeError("s must either be an instance of quatmath.quat or a " \
                + "subclass, or be an integer or floating-point number.")
                
        if isinstance(s, quat):
            return q * s.inverse()
        
        else:  # numeric type (Note: Other types are already caught above.)
            return q * (1.0/s)
    
    
    @classmethod
    def _mod(cls, q, modulus):
        return cls([i%modulus for i in q.quatVector()])


    @classmethod
    def _areQuatAxesParallel(cls, q, s, threshold = DEF_THRESHOLD):
        """
        Returns whether two quats have parallel axes, within a threshold. To force
        an exact match on the axes, pass threshold = 0.0, but this is not
        recommended due to floating-point rounding errors. Note: If q or s is
        real-valued, this function returns True.
        """
                
        if isinstance(q, NUMERIC_TYPE) or isinstance(s, NUMERIC_TYPE):
            return True
    
        # Are q & s in angle-axis form?        
        elif q._axis != None and s._axis != None:
            axesDot = np.dot(q._axis, s._axis)
            return mostlyEqual(axesDot, 1.0, threshold) \
                   or mostlyEqual(axesDot, -1.0, threshold)
            
        else:
            if q._qvec == None:
                q._calcQuatVector()
            if s._qvec == None:
                s._calcQuatVector()
                
            qImagVec = q.imagVector()
            sImagVec = s.imagVector()
            
            qVecNormSq = np.dot(qImagVec, qImagVec)
            sVecNormSq = np.dot(sImagVec, sImagVec)
            
            vectorsDot = np.dot(qImagVec, sImagVec)
            return mostlyEqual(vectorsDot * vectorsDot, \
                               qVecNormSq * sVecNormSq, \
                               threshold)


    # Note: q_qvec & s_qvec are quatVectors, not quats.
    @classmethod
    def _multiplyQuatVecForm(cls, q_qvec, s_qvec):
        assert q_qvec != None and s_qvec != None    
                
        # Handle quatVec form quats:                  
        realScalarPart = q_qvec[0] * s_qvec[0]
        # Dot-product of imag. vectors
        realDotPart = sum([q_qvec[i] * s_qvec[i] for i in range(1,4) \
                           if not mostlyEqual(q_qvec[i], 0.0) \
                              and not mostlyEqual(s_qvec[i], 0.0)])
        realPart = realScalarPart - realDotPart

        # Calc. cross-product of imag. vectors.
        vecCrossPart = np.array( \
                [q_qvec[2] * s_qvec[3] - q_qvec[3] * s_qvec[2], \
                 q_qvec[3] * s_qvec[1] - q_qvec[1] * s_qvec[3], \
                 q_qvec[1] * s_qvec[2] - q_qvec[2] * s_qvec[1]])
                 
        # Calc. sum of scalar products. i.e.:
        # q.realPart() * s.imagVector() + s.realPart() * q.imagVector()
        vecScalarPart = np.array( \
                [q_qvec[0] * s_qvec[1] + s_qvec[0] * q_qvec[1], \
                 q_qvec[0] * s_qvec[2] + s_qvec[0] * q_qvec[2], \
                 q_qvec[0] * s_qvec[3] + s_qvec[0] * q_qvec[3]])
        
        vecPart = vecCrossPart + vecScalarPart
        quatVec = [realPart] + [vc for vc in vecPart]
            
        return cls(quatVec)
    
    
    @classmethod
    def _multiplyAngleAxisForm(cls, q, s):
        assert isinstance(q, quat) and isinstance(s, quat)
        assert q._axis != None and s._axis != None
        assert q._angle != None and s._angle != None
        assert q._scaling != None and s._scaling != None
        
        if cls._areQuatAxesParallel(q, s):
            oppAxes = not mostlyEqual(q._axis, s._axis)
            newFullAngle = 2.0 * (q._angle + (-s._angle if oppAxes else s._angle))
            
            return cls(angle = newFullAngle, \
                       axisVec = q._axis, \
                       scaling = q._scaling * s._scaling)
        else:
            qQuatVec = q._calcQuatVector()
            sQuatVec = s._calcQuatVector()
            
            return cls._multiplyQuatVecForm(qQuatVec, sQuatVec)


def mostlyEqual(a, b, threshold = DEF_THRESHOLD):
    """
    Performs an near-equality check on integer or floating-point values,
    considering the values equal as long as their absolute difference is below
    a threshold value.
    """
    
    if not isinstance(a, NUMERIC_TYPE) or not isinstance(b, NUMERIC_TYPE):        
        raise TypeError(\
            "Both a & b must be integer or floating-point numbers.")
    
    return abs(a - b) <= threshold



class versor(quat):
    """ A unitary quaternion (a.k.a. versor) subclass of quat, which supports
        additional features related to performing 3D rotations. """        
    
    @classmethod
    def _quat(cls):
        return quat
        
    @classmethod
    def _versor(cls):
        ##If you subclass versor, it will be the versor of itself. 
        return cls
        
    def __init__(self, *args, **kwargs):
        if 'rotationVec' in kwargs:
            rotVec = kwargs['rotationVec']
            
            # Copy rotation vector.
            if not isinstance(rotVec, np.array):
                rotVec = np.array(rotVec[:])
            else:
                rotVec = np.copy(rotVec)
                
            angle = la.norm(rotVec)   # The rotation angle is the vector's norm
            axisVec = rotVec / angle  # Axis of rotation as unitary vector
            
            super(versor, self).__init__(angle = angle, axisVec = axisVec)
            
        else:
            super(versor, self).__init__(*args, **kwargs)
            norm = super(versor, self).norm()
            
            if mostlyEqual(norm, 0.0):
                raise ZeroQuatError()
            # Normalize the versor if it's currently non-unitary.
            elif not mostlyEqual(norm, 1.0):
                temp = super(versor, self).__div__(norm)
                super(versor, self).__init__(inQuat = temp)
            # else: No normalization needed as the versor is already unitary.
    
    def pow(self, exponent):
        """
        Applies the given real exponent to this versor.
        (This overrides the pow() method of the parent quat class, with a more
        efficient version that uses the fact that versors are unitary quats.)
        """
        
        if isinstance(exponent, NUMERIC_TYPE):
            if exponent == 1.0:
                return self
            elif exponent == 0.0:
                return versor.one()
            
            imagVec = self.imagVector()
            realPart = self.realPart()
            imagNorm = la.norm(imagVec)
            if not self.isRealValued():
                normalVec = imagVec / imagNorm
            else:
                # This will make sure the output is purely complex.
                normalVec = np.array([1, 0, 0])
                
            # Note: This is more numerically stable than math.acos(q.realPart()).
            theta = math.atan2(imagNorm, realPart)
            newFullAngle = 2.0 * theta * exponent
            
            return versor(angle = newFullAngle, axisVec = normalVec)
        
        else:
            raise TypeError( \
                "exponent must be an integer or floating-point number.")
    
    
    def applyRotation(self, vector):
        """
        Applies the rotation represented by this versor to the given vector.
        """        
        
        if any([not isinstance(vc, NUMERIC_TYPE) for vc in vector]):
            raise TypeError("vector must be a 3D vector w/ integer or "\
                + "floating-point components.")
        
        # Convert vector into a pure imaginary quaternion.
        qVec = versor(imagVec = vector)
        # Apply this versor's rotation.
        qRotated = self * qVec * self.conj()  # Note: Result is a quat.
        
        # Return the rotated vector.
        return qRotated.imagVector()
        
    
    def toRotationVector(self):
        """
        Returns a versor's equivalent "rotation" 3D vector, which is formed by
        taking this versor's axis of rotation and multiplying it by the angle
        of rotation.
        """
        
        # Use _calcAngleAxis() for efficiency, i.e. instead of calling angle()
        # & axis() separately.
        angleAxisTuple = self._calcAngleAxis()
        angle = 2.0 * angleAxisTuple[0]  # _calcAngleAxis() returns half-angle
        # Make copy of axis since _calcAngleAxis() does not.
        axisVec = np.copy(angleAxisTuple[1])
        
        return angle * axisVec
        
    
    @classmethod
    def fromQuat(cls, quat):
        """
        Factory function that creates a versor from the given quat, by normalizing
        it if it's non-unitary.
        """
        return cls(inQuat = quat)
    
    
    @classmethod
    def fromRotationVector(cls, rotationVector):
        """
        Factory function that creates a versor (rotation quaternion), given
        a "rotation" 3-vector, which specifies rotation about x, y & z-axes in its
        components. This factory function is esp. useful for creating an angular
        velocity versor, given a 3-vector of angular velocities (in radians/sec)
        about x, y & z-axes, such as obtained from a 3-axis rate gyroscope.
        """        
        return cls(rotationVec = rotationVector)
        
        
    @classmethod
    def fromVectorDivision(cls, vector1, vector2):
        """
        This factory function produces a versor (i.e., rotation quaternion), as
        the "quotient" of the two 3D vectors provided. In particular, when the
        resulting versor is applied (via conjugation,
        i.e.: q * divisorQ * q**-1) to a pure-imaginary quaternion formed from
        vector2 (the divisor), the resulting quaternion's imaginary part is
        vector2 rotated so that it is parallel with vector1 (the dividend).
        Also, if the 2 vectors are of equal magnitude (i.e., 2-norms), then the
        result of this conjugation will equal vector1 (i.e., the dividend).
        """
        
        if len(vector1) != 3 or len(vector2) != 3:
            raise TypeError('vector1 & vector2 must be numpy arrays '\
                + 'or sequences of length 3.')
                
        # Handle any length-3 sequence, not just numpy arrays.
        if not isinstance(vector1, np.array):
            vector1 = np.array([dc for dc in vector1])
        if not isinstance(vector2, np.array):
            vector2 = np.array([dc for dc in vector2])
            
        norm1 = la.norm(vector1)
        norm2 = la.norm(vector2)
        normProd = norm1 * norm2
        dotProd = np.dot(vector1, vector2)
        crossProdVec = np.cross(vector2, vector1)
        cosTheta = dotProd / normProd
        sinTheta = la.norm(crossProdVec / normProd)
        halfAngle = math.atan2(sinTheta, cosTheta)
        normalVec = crossProdVec / (normProd * sinTheta)
        
        return cls(angle = 2.0 * halfAngle, axisVec = normalVec)
